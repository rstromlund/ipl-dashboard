package net.stromlunds.ipldashboard.repository;

import org.springframework.data.repository.CrudRepository;
import net.stromlunds.ipldashboard.model.Team;

public interface TeamRepository extends CrudRepository<Team, Long> {
	Team findByTeamName(String teamName);
}
