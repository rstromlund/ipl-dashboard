package net.stromlunds.ipldashboard.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import net.stromlunds.ipldashboard.model.Team;
import net.stromlunds.ipldashboard.repository.MatchRepository;
import net.stromlunds.ipldashboard.repository.TeamRepository;

@RestController
@CrossOrigin
public class TeamController {
	public TeamController(TeamRepository teamRepository, MatchRepository matchRepository) {
		this.teamRepository = teamRepository;
		this.matchRepository = matchRepository;
	}

	private TeamRepository teamRepository;
	private MatchRepository matchRepository;

	@GetMapping("/team/{teamName}")
	public Team getTeam(@PathVariable String teamName) {
		Team team = teamRepository.findByTeamName(teamName);
		team.setMatches(matchRepository.findLastestMatchesByTeam(teamName, 4));
		return team;
	}
}
