package net.stromlunds.ipldashboard.data;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import net.stromlunds.ipldashboard.model.Team;

@Slf4j
@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {
	private final EntityManager em;

	@Autowired
	public JobCompletionNotificationListener(EntityManager em) {
		this.em = em;
	}

	@Override
	@Transactional
	public void afterJob(JobExecution jobExecution) {
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			log.info("!!! JOB FINISHED! Time to verify the results");

			Map<String, Team> teamData = new HashMap<>();

			em.createQuery("SELECT m.team1, count(*) FROM Match m GROUP BY m.team1", Object[].class).getResultList()
					.stream().forEach(e -> {
						String teamName = (String) e[0];
						teamData.put(teamName, new Team(teamName, (long) e[1]));
					});

			em.createQuery("SELECT m.team2, count(*) FROM Match m GROUP BY m.team2", Object[].class).getResultList()
					.stream().forEach(e -> {
						Team team = teamData.get(e[0]);
						team.setTotalMatches(team.getTotalMatches() + (long) e[1]);
					});

			em.createQuery("SELECT m.matchWinner, count(*) FROM Match m GROUP BY m.matchWinner", Object[].class).getResultList()
					.stream().forEach(e -> {
						Team team = teamData.get(e[0]);
						if (team != null)
							team.setTotalWins((long) e[1]);
					});

			teamData.values().forEach(team -> em.persist(team));
			teamData.values().forEach(team -> System.out.println(team));
		}
	}
}
