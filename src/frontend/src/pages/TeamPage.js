import { React, useEffect, useState } from 'react';
import { MatchSmallCard } from '../components/MatchSmallCard';
import { MatchDetailCard } from '../components/MatchDetailCard';

export const TeamPage = () => {
	const [team, setTeam] = useState([]);

	useEffect(() => {
		const fetchMatches = async () => {
			const response = await fetch('http://localhost:8080/team/Rajasthan%20Royals');
			const data = await response.json();
			setTeam(data);
		}
		fetchMatches();
	}, []);

	if (!team || !team.teamName) {
		return <h1>Team not found</h1>
	}

	return (
		<div className="TeamPage">
			<h1>{team.teamName}</h1>
			<MatchDetailCard match={team.matches[0]} />
			{team.matches.slice(1).map(match => <MatchSmallCard match={match} />)}
		</div>
	);
}
